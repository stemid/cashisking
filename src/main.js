import Vue from 'vue'
import App from './App.vue'
import vuetify from '@/plugins/vuetify';
import store from '@/store'
import '@/scss/style.scss'

Vue.config.productionTip = false

new Vue({
  components: { App },
  render: h => h(App),
  vuetify,
  store,
}).$mount('#app')
