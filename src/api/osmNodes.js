import config from '@/config'
import axios from 'axios'

/*
 * @param {Array} bounds Array of left, bottom, right, top coordinates
 * boundary.
 */
export const apiRequestNodesByBounds = (bounds) => new Promise((resolve, reject) => {
  var tag = 'payment:cash=yes'
  var apiUrl = `${config.apiUrl}?node[${tag}][bbox=${bounds.join(',')}]`
  //
  var p = axios.get(apiUrl).then((resp) => {
    resolve(resp)
  }).catch((error) => {
    reject(error)
  })

  return p
})
