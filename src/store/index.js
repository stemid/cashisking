import Vue from 'vue'
import Vuex from 'vuex'

import osmNodes from '@/store/modules/osmNodes'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    osmNodes,
  },
  //strict: debug,
  strict: false // this is necessary to avoid an error when editing forms
                // that contain vuex state data.
})
