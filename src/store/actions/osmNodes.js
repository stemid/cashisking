export const NODES_REQUEST = 'nodes/request'
export const NODES_REQUEST_SUCCESS = 'nodes/request/success'
export const NODES_REQUEST_ERROR = 'nodes/request/error'
