import Vue from 'vue'
var parseString = require('xml2js').parseString

import { 
  NODES_REQUEST,
  NODES_REQUEST_SUCCESS,
  NODES_REQUEST_ERROR,
} from '../actions/osmNodes'

import {
  apiRequestNodesByBounds
} from '../../api/osmNodes'

const state = { 
  nodes: [],

  requestStatus: '',
  requestError: '',
}

const getters = {
  /*
  getNodeById: (state) => (nodeId) => {
    for (let i=0;i<state.nodes.length;i++) {
      if (nodeId == state.nodes[i].node_id)
        return state.nodes[i]
    }
  },
  */
}

const actions = {
  [NODES_REQUEST]: ({commit}, bounds) => {
    commit(NODES_REQUEST)
    var p = apiRequestNodesByBounds(bounds)

    p.then((resp) => {
      console.log(resp)
      parseString(resp.data, (err, result) => {
        if (err) {
          commit(NODES_REQUEST_ERROR, err)
        } else {
          commit(NODES_REQUEST_SUCCESS, resp)
        }
      })
    }).catch((error) => {
      commit(NODES_REQUEST_ERROR, error)
    })

    return p
  },

}

const mutations = {
  [NODES_REQUEST]: (state) => {
    state.requestStatus = 'loading'
  },
  [NODES_REQUEST_SUCCESS]: (state, resp) => {
    Vue.set(state, 'nodes', resp.data)
    state.requestStatus = 'success'
  },
  [NODES_REQUEST_ERROR]: (state, error) => {
    state.requestError = error
  },

}

export default {
  state,
  getters,
  actions,
  mutations,
}
