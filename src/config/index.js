export default {
  locale: 'sv',
  apiUrl: process.env.VUE_APP_API_URL,
  tileLayerUrl: process.env.VUE_APP_TILE_LAYER_URL,
  mapboxAccessToken: process.env.VUE_APP_MAPBOX_ACCESS_TOKEN || '',
  mapboxId: process.env.VUE_APP_MAPBOX_ID || '',
}
