# cashisking

# Project setup
```
yarn install
```

# Compiles and hot-reloads for development
```
yarn serve
```

# Compiles and minifies for production
```
yarn build
```

# Lints and fixes files
```
yarn lint
```

# Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

Set environment configuration variables in .env files. For example .env.staging is read by `` yarn build --mode staging``.

  * ``VUE_APP_API_URL`` - URL to Overpass XAPI for OpenStreetMap
  * ``VUE_APP_TILE_LAYER_URL`` - URL to a tile layer
  * ``VUE_APP_MAPBOX_ACCESS`` - Access key for Mapbox API if you want to use their tiles
  * ``VUE_APP_MAPBOX_ID`` - Mapbox ID like ``mapbox/streets-v11`` or ``mapbox.streets``.

## Tile layer URL

Some examples;

  * https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png
  * https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken} - Requires mapbox ID and access token set
  * https://{s}.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png
